<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AlunoController extends Controller
{
    public function inicio(){
        return view('alunos.inicio');
    }

    public function editar(){
        return view('alunos.editar');
    }

    public function criar(){
       return view('alunos.criar');
    }
}
